//
//  ApiEndpoints.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

struct ApiEndpoints {
    private static var baseUrl = "https://www.getyourguide.com"
    private static var reviewsPath = "reviews.json"
    
    static func reviewsUrl(tourUrl: String) -> String {
        return String(format: "%@/%@/%@", baseUrl, tourUrl, reviewsPath)
    }
}
