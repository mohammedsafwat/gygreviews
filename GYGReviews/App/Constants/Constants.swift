//
//  Constants.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    struct GeneralAppProperties {
        struct ActivityIndicator {
            static let Size = CGSize(width: 35.0, height: 35.0)
            static let ColorHex = "#da4c49"
        }

        struct ErrorAlert {
            static let Title = "Error"
            static let DismissActionTitle = "Ok"
            static let TextColorHex = "#333333"
            static let TextFontName = "Helvetica"
            static let TextFontSize = CGFloat(16.0)
        }
    }

    struct API {
        struct BerlinTour {
            static let TourName = "berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776"
        }

        struct ReviewsResponseKeys {
            static let Data = "data"
            static let Id = "review_id"
            static let Rating = "rating"
            static let Title = "title"
            static let Message = "message"
            static let Author = "author"
            static let ForeginLanguage = "foreignLanguage"
            static let Date = "date"
            static let LangaugeCode = "languageCode"
            static let TravelerType = "traveler_type"
            static let ReviewerName = "reviewerName"
            static let ReviewerCountry = "reviewerCountry"
        }
    }

    struct ViewControllers {
        struct Reviews {
            static let ViewControllerTitle = "Reviews"
            static let ReviewsPerPage = 20
            static let RefreshableFooterHeight = CGFloat(70.0)

            struct TableView {
                static let CellName = "ReviewsTableViewCell"
                static let CellIdentifier = "ReviewsTableViewCell"
                static let SectionHeaderHeight = CGFloat(6.0)
            }

            struct TableViewCell {
                static let CornerRadius = CGFloat(8.0)
                static let CardShadowRadius = CGFloat(14.0)
                static let CardShadowOpacity: Float = 1.0
                static let CardShadowOffset = CGSize(width: 0.0, height: 0.5)
                static let CardShadowColorHex = "#B8B8B8"
                static let CardShadowColorAlpha = CGFloat(0.5)
            }

            struct SortActionSheet {
                static let Title = "Sort"
                static let Message = "Sort Reviews By"
                static let DismissActionTitle = "Dismiss"
                static let SortByDateTitle = "Date"
                static let SortByRatingTitle = "Rating"
                static let TextColorHex = "#333333"
                static let TextFontName = "Helvetica"
                static let TextFontSize = CGFloat(18.0)
            }
        }
    }
}
