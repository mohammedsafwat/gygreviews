//
//  RestNetworkClient.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import RxSwift
import Alamofire
import RxAlamofire

class RestNetworkClient: RestNetworkClientProtocol {
    private var headers: [String: String]
    private let disposeBag = DisposeBag()
    
    init(headers: [String: String]) {
        self.headers = headers
    }
    
    func performRequest(requestURLString: String, type: HTTPRequestType, parameters: [String: Any]) -> Observable<Result<(HTTPURLResponse, Any), DataError>> {
        return Observable<Result<(HTTPURLResponse, Any), DataError>>.create({ (observer) -> Disposable in
            if let url = URL(string: requestURLString) {
                RxAlamofire.requestJSON(type == .get ? .get : .post, url, parameters: parameters, headers: self.headers).subscribe(onNext: { result in
                    observer.onNext(.success(result))
                    observer.onCompleted()
                }, onError: { error in
                    observer.onError(error)
                }).disposed(by: self.disposeBag)
            } else {
                observer.onError(DataErrorHelper.requestFailedError)
            }
            return Disposables.create()
        })
    }
}
