//
//  DataErrorHelper.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class DataErrorHelper {
    class var dbFailedError: DataError {
        let errorMessage = DataErrorMessageFormatter.getErrorMessage(errorType: .dbFailed, errorMessage: "")
        let dataError = DataError(dataErrorType: .dbFailed, dataErrorMessage: errorMessage)
        return dataError
    }
    
    class var localOperationFailedError: DataError {
        let errorMessage = DataErrorMessageFormatter.getErrorMessage(errorType: .localOperationFailed, errorMessage: "")
        let dataError = DataError(dataErrorType: .localOperationFailed, dataErrorMessage: errorMessage)
        return dataError
    }
    
    class var parseError: DataError {
        let errorMessage = DataErrorMessageFormatter.getErrorMessage(errorType: .parse, errorMessage: "")
        let dataError = DataError(dataErrorType: .parse, dataErrorMessage: errorMessage)
        return dataError
    }
    
    class var requestFailedError: DataError {
        let errorMessage = DataErrorMessageFormatter.getErrorMessage(errorType: .requestFailed, errorMessage: "")
        let dataError = DataError(dataErrorType: .requestFailed, dataErrorMessage: errorMessage)
        return dataError
    }
    
    class var requestCancelledError: DataError {
        let errorMessage = DataErrorMessageFormatter.getErrorMessage(errorType: .cancelled, errorMessage: "")
        let dataError = DataError(dataErrorType: .requestFailed, dataErrorMessage: errorMessage)
        return dataError
    }
}
