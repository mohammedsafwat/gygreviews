//
//  DataErrorMessageFormatter.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class DataErrorMessageFormatter {
    static func getErrorMessage(errorType: DataErrorType, errorMessage: String?) -> String? {
        if (errorMessage ?? "").isEmpty {
            return getErrorMessageString(errorType: errorType)
        }
        return errorMessage
    }
    
    private static func getErrorMessageString(errorType: DataErrorType) -> String {
        switch errorType {
        case .noConnection:
            return errorType.errorMessage
        case .serverFailed:
            return errorType.errorMessage
        case .requestFailed:
            return errorType.errorMessage
        default:
            return errorType.errorMessage
        }
    }
}
