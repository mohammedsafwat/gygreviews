//
//  DataErrorType.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

enum DataErrorType {
    case noConnection
    case timeout
    case serverFailed
    case requestFailed
    case parse
    case unauthorized
    case dbFailed
    case localOperationFailed
    case cancelled
    case unknown
    
    var errorTitle: String {
        switch self {
        case .noConnection:
            return "No internet connection"
        case .timeout:
            return "Operation time out"
        case .requestFailed, .serverFailed, .parse, .unauthorized, .cancelled:
            return "Error"
        case .dbFailed, .localOperationFailed:
            return "Error"
        default:
            return "Error"
        }
    }
    
    var errorMessage: String {
        switch self {
        case .noConnection:
            return "Please check that you internet connection is active"
        case .timeout:
            return "The operation took longer than expected. Please try again later"
        case .requestFailed, .serverFailed, .parse, .unauthorized, .cancelled:
            return "Oops. An unexpected error happend. Please try again later"
        case .dbFailed, .localOperationFailed:
            return "Oops. It seems that something wrong happend. Please try again"
        default:
            return "Oops. An unexpected error happend. Please try again later"
        }
    }
}
