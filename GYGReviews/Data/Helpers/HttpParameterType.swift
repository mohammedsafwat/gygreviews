//
//  HttpParameterType.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

enum HttpParameterType: String {
    case count
    case page
    case rating
    case type
    case sortBy
    case direction
}
