//
//  ReviewsDataLoadingType.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

enum ReviewsDataLoadingType {
    case normal
    case infinite
}
