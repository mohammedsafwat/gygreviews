//
//  ReviewsSortDirection.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

enum ReviewsSortDirection: String {
    case ascending = "asc"
    case descending = "desc"
}
