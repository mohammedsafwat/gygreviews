//
//  ReviewsSortType.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

enum ReviewsSortType: String {
    case dateOfReview = "date_of_review"
    case rating = "rating"
}
