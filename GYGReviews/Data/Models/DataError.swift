//
//  DataError.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class DataError: Error {
    
    // MARK: - Properties
    
    var dataErrorType: DataErrorType
    var dataErrorMessage: String?
    
    // MARK: - Memberwise Initializer
    
    init(dataErrorType: DataErrorType, dataErrorMessage: String?) {
        self.dataErrorType = dataErrorType
        self.dataErrorMessage = dataErrorMessage
    }
}
