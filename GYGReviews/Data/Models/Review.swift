//
//  Review.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation
import ObjectMapper

class Review: Mappable {

    // MARK: - Properties

    var id: Int64?
    var rating: String?
    var title: String?
    var message: String?
    var author: String?
    var foreignLanguage: Bool?
    var date: String?
    var languageCode: String?
    var travelerType: String?
    var reviewerName: String?
    var reviewerCountry: String?

    var titleFormatted: String {
        return title ?? "--"
    }
    var messageFormatted: String {
        return message ?? "--"
    }
    var ratingAsInt: Int {
        return Int(Double(rating ?? "") ?? 0.0)
    }

    // MARK: - Initializer
    
    init(id: Int64?, rating: String?, title: String?, message: String?, author: String?, foreignLanguage: Bool?, date: String?, languageCode: String?, travelerType: String?, reviewerName: String?, reviewerCountry: String?) {
        self.id = id
        self.rating = rating
        self.title = title
        self.message = message
        self.author = author
        self.foreignLanguage = foreignLanguage
        self.date = date
        self.languageCode = languageCode
        self.travelerType = travelerType
        self.reviewerName = reviewerName
        self.reviewerCountry = reviewerCountry
    }

    // MARK: - Mapping

    required init?(map: Map) {
        mapping(map: map)
    }

    func mapping(map: Map) {
        id <- map[Constants.API.ReviewsResponseKeys.Id]
        rating <- map[Constants.API.ReviewsResponseKeys.Rating]
        title <- map[Constants.API.ReviewsResponseKeys.Title]
        message <- map[Constants.API.ReviewsResponseKeys.Message]
        author <- map[Constants.API.ReviewsResponseKeys.Author]
        foreignLanguage <- map[Constants.API.ReviewsResponseKeys.ForeginLanguage]
        date <- map[Constants.API.ReviewsResponseKeys.Date]
        languageCode <- map[Constants.API.ReviewsResponseKeys.LangaugeCode]
        travelerType <- map[Constants.API.ReviewsResponseKeys.TravelerType]
        reviewerName <- map[Constants.API.ReviewsResponseKeys.ReviewerName]
        reviewerCountry <- map[Constants.API.ReviewsResponseKeys.ReviewerCountry]
    }
}
