//
//  ReviewsRequest.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class ReviewsRequest {

    // MARK: - Properties

    var count: Int
    var page: Int
    var rating: Int?
    var sortType: ReviewsSortType?
    var sortDirection: ReviewsSortDirection?
    var dataLoadingType: ReviewsDataLoadingType = .normal

    // MARK: - Initializer

    init(count: Int, page: Int) {
        self.count = count
        self.page = page
    }

    func equals(reviewsRequest: ReviewsRequest) -> Bool {
        return self.count == reviewsRequest.count &&
            self.page == reviewsRequest.page &&
            self.rating == reviewsRequest.rating &&
            self.sortType?.rawValue == reviewsRequest.sortType?.rawValue &&
            self.sortDirection?.rawValue == reviewsRequest.sortDirection?.rawValue
    }
}
