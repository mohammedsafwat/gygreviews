//
//  DataModule.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class DataModule {
    static let shared = DataModule()
    private let restNetworkClient = HttpClientModule.shared.httpClient

    func reviewsRepository() -> ReviewsDataSource {
        let reviewsRemoteDataSource = ReviewsModule.shared.reviewsRemoteDataSource(restNetworkClient: restNetworkClient)
        return ReviewsRepository(remoteDataSource: reviewsRemoteDataSource)
    }
}
