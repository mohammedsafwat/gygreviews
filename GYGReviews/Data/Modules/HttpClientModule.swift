//
//  HttpClientModule.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class HttpClientModule {
    static let shared = HttpClientModule()
    var httpClient: RestNetworkClientProtocol

    private init() {
        httpClient = RestNetworkClient(headers: [:])
    }
}
