//
//  ReviewsModule.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 30.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class ReviewsModule {
    static let shared = ReviewsModule()

    func reviewsRemoteDataSource(restNetworkClient: RestNetworkClientProtocol) -> ReviewsDataSource {
        return ReviewsRemoteDataSource(restNetworkClient: restNetworkClient)
    }
}
