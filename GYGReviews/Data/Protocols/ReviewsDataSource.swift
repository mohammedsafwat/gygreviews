//
//  ReviewsDataSource.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import RxSwift

protocol ReviewsDataSource {
    func reviews(reviewsRequest: ReviewsRequest) -> Observable<[Review]>
}
