//
//  ReviewsRemoteDataSource.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import RxSwift
import ObjectMapper

class ReviewsRemoteDataSource: ReviewsDataSource {

    // MARK: - Properties

    private var restNetworkClient: RestNetworkClientProtocol

    // MARK: - Initializer

    init(restNetworkClient: RestNetworkClientProtocol) {
        self.restNetworkClient = restNetworkClient
    }

    // MARK: - ReviewsDataSource Methods

    func reviews(reviewsRequest: ReviewsRequest) -> Observable<[Review]> {
        let count = reviewsRequest.count
        let page = reviewsRequest.page
        let rating = reviewsRequest.rating ?? 0
        let sortType = reviewsRequest.sortType ?? .dateOfReview
        let sortDirection = reviewsRequest.sortDirection ?? .ascending

        let parameters: [String: Any] = [HttpParameterType.count.rawValue: count, HttpParameterType.page.rawValue: page, HttpParameterType.rating.rawValue: rating, HttpParameterType.sortBy.rawValue: sortType.rawValue, HttpParameterType.direction.rawValue: sortDirection.rawValue]
        let requestURLString = ApiEndpoints.reviewsUrl(tourUrl: Constants.API.BerlinTour.TourName)

        return restNetworkClient.performRequest(requestURLString: requestURLString, type: .get, parameters: parameters).flatMap({ result in
            try self.parseReviewsResponse(result: result)
        })
    }
}

// MARK: - Parsing Methods

extension ReviewsRemoteDataSource {
    private func parseReviewsResponse(result: Result<(HTTPURLResponse, Any), DataError>) throws -> Observable<[Review]> {
        switch result {
        case .success(let response):
            guard let responseData = response.1 as? [String: Any],
                let reviewsData = responseData[Constants.API.ReviewsResponseKeys.Data] as? [[String: Any]] else {
                throw DataErrorHelper.parseError
            }
            let reviews = reviewsData.compactMap { reviewData in
                return self.parseReview(reviewData: reviewData)
            }.compactMap {$0}
            return Observable<[Review]>.just(reviews)
        case .failure(let error):
            throw error
        }
    }

    private func parseReview(reviewData: [String: Any]) -> Review? {
        return Review(JSON: reviewData)
    }
}
