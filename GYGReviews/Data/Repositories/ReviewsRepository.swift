//
//  ReviewsRepository.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import RxSwift

class ReviewsRepository: ReviewsDataSource {
    private var remoteDataSource: ReviewsDataSource
    
    init(remoteDataSource: ReviewsDataSource) {
        self.remoteDataSource = remoteDataSource
    }
    
    func reviews(reviewsRequest: ReviewsRequest) -> Observable<[Review]> {
        return remoteDataSource.reviews(reviewsRequest: reviewsRequest)
    }
}
