//
//  UIBarButtonItem.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 30.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    func addAction(action: Selector, to target: UIViewController) {
        self.target = target
        self.action = action
    }
}
