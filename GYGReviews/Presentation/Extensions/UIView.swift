//
//  UIView.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 30.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit

extension UIView {
    func addShadow(shadow: Shadow) {
        let layer = self.layer
        layer.shadowRadius = shadow.radius
        layer.shadowOpacity = shadow.opacity
        layer.shadowOffset = shadow.offset
        layer.masksToBounds = shadow.masksToBounds
        layer.shadowColor = shadow.shadowColor.cgColor
        let cachedBackgroundColor = self.backgroundColor?.cgColor
        self.backgroundColor = nil
        layer.backgroundColor =  cachedBackgroundColor
    }

    func setRoundedCorners(cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
}
