//
//  ActivityIndicatorType.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 08.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

enum ActivityIndicatorType {
    case ballClipRotate
    case ballBeat
}
