//
//  Alert.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit

class Alert {
    private var viewController: UIViewController
    private var title: String
    private var message: String
    private var textColorHex: String
    private var textFontName: String
    private var textFontSize: CGFloat
    private var style: UIAlertController.Style
    private var actions: [UIAlertAction]
    private let attributedTitleKey = "attributedTitle"
    private let attributedMessageKey = "attributedMessage"

    init(viewController: UIViewController, title: String, message: String, textColorHex: String, textFontName: String, textFontSize: CGFloat, style: UIAlertController.Style, actions: [UIAlertAction]) {
        self.viewController = viewController
        self.title = title
        self.message = message
        self.textColorHex = textColorHex
        self.textFontName = textFontName
        self.textFontSize = textFontSize
        self.style = style
        self.actions = actions
    }

    func show() {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)

        let textFont = UIFont(name: textFontName, size: textFontSize) ?? UIFont.systemFont(ofSize: textFontSize)
        let textColor = UIColor(hexString: textColorHex)
        let textAttributes = [NSAttributedString.Key.font: textFont, NSAttributedString.Key.foregroundColor: textColor]

        let titleAttributedString = NSAttributedString(string: title, attributes: textAttributes)
        let messageAttributedString = NSAttributedString(string: message, attributes: textAttributes)

        alertController.setValue(titleAttributedString, forKey: attributedTitleKey)
        alertController.setValue(messageAttributedString, forKey: attributedMessageKey)

        actions.forEach { alertController.addAction($0) }

        viewController.present(alertController, animated: true, completion: nil)
    }
}
