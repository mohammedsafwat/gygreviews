//
//  Shadow.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 30.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit

class Shadow {
    var radius: CGFloat
    var opacity: Float
    var offset: CGSize
    var masksToBounds: Bool
    var shadowColor: UIColor

    init(radius: CGFloat, opacity: Float, offset: CGSize, masksToBounds: Bool, shadowColor: UIColor) {
        self.radius = radius
        self.opacity = opacity
        self.offset = offset
        self.masksToBounds = masksToBounds
        self.shadowColor = shadowColor
    }
}
