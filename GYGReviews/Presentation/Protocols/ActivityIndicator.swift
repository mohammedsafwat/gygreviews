//
//  ActivityIndicator.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 08.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit

protocol ActivityIndicator {
    func startAnimating()
    func stopAnimating()
    func translatesAutoresizingMaskIntoConstraints(translates: Bool)
    func setCenter(center: CGPoint)
}
