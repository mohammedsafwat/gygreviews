//
//  ViewModelNetworking.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

protocol ViewModelNetworking {
    func refresh()
    func loadMore()
    func tryAgain()
}
