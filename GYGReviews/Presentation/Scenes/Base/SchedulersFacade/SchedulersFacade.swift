//
//  SchedulersFacade.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import RxSwift

protocol SchedulersFacade {
    func backgroundScheduler() -> SchedulerType
    func mainScheduler() -> SchedulerType
}
