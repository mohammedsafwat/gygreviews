//
//  SchedulersFacadeImpl.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import RxSwift

class SchedulersFacadeImpl: SchedulersFacade {

    // Background thread scheduler
    func backgroundScheduler() -> SchedulerType {
        return ConcurrentDispatchQueueScheduler(qos: .background)
    }

    // Main thread scheduler
    func mainScheduler() -> SchedulerType {
        return MainScheduler.instance
    }
}
