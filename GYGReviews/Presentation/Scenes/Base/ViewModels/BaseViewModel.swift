//
//  BaseViewModel.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation
import RxSwift

class BaseViewModel {

    // MARK: - Properties

    public var isLoading = PublishSubject<Bool>()
    public var error = PublishSubject<Error>()
}
