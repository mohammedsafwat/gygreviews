//
//  ReviewsViewController.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 28.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit
import RxSwift

class ReviewsViewController: UIViewController {

    // MARK: - Properties

    @IBOutlet private weak var reviewsTableView: UITableView!
    @IBOutlet private weak var sortBarButtonItem: UIBarButtonItem!
    private let disposeBag = DisposeBag()

    private lazy var viewModel: ReviewsViewModel = {
        let reviewsDataSource = DataModule.shared.reviewsRepository()
        let schedulersFacade = SchedulersFacadeImpl()

        return ReviewsViewModel(reviewsDataSource: reviewsDataSource, schedulersFacade: schedulersFacade, disposeBag: disposeBag)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupDataBinding()

        viewModel.setupReviewsRequestObserver()
    }
}

// MARK: - ReviewsTableView DataSource

extension ReviewsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ViewControllers.Reviews.TableView.CellIdentifier, for: indexPath) as? ReviewsTableViewCell else {
            fatalError("Unexpected ReviewsTableView Cell")
        }

        let review = viewModel.review(for: indexPath.row)
        cell.configure(withReview: review)
        return cell
    }
}

// MARK: - ReviewsTableView Delegate

extension ReviewsViewController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
}

// MARK: - UI Setup Methods

extension ReviewsViewController {
    private func setupUI() {
        setupViewControllerTitle()
        setupSortBarButtonItem()
        setupReviewsTableView()
        setupRefreshControl()
        setupInfiniteLoadingFooter()
    }

    private func setupViewControllerTitle() {
        self.title = Constants.ViewControllers.Reviews.ViewControllerTitle
    }

    private func setupSortBarButtonItem() {
        sortBarButtonItem.addAction(action: #selector(didTapSortBarButtonItem), to: self)
    }

    private func setupReviewsTableView() {
        reviewsTableView.register(UINib(nibName: Constants.ViewControllers.Reviews.TableView.CellName, bundle: nil), forCellReuseIdentifier: Constants.ViewControllers.Reviews.TableView.CellIdentifier)
        reviewsTableView.dataSource = self
        reviewsTableView.delegate = self
        reviewsTableView.rowHeight = UITableView.automaticDimension
        reviewsTableView.separatorStyle = .none
        reviewsTableView.tableFooterView = UIView()
    }

    private func setupRefreshControl() {
        let refreshControl = UIRefreshControl()
        reviewsTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }

    private func setupInfiniteLoadingFooter() {
        let activityIndicatorFrame = CGRect(origin: CGPoint.zero, size: Constants.GeneralAppProperties.ActivityIndicator.Size)
        let activityIndicatorColor = UIColor(hexString: Constants.GeneralAppProperties.ActivityIndicator.ColorHex)
        let activityIndicatorPadding = CGFloat(0.0)
        let activityIndicator = ActivityIndicatorImpl(frame: activityIndicatorFrame, type: .ballClipRotate, color: activityIndicatorColor, padding: activityIndicatorPadding)

        let refreshFooter = ReviewsInfiniteLoadingFooter(frame: CGRect(x: 0, y: 0, width: reviewsTableView.frame.size.width, height: Constants.ViewControllers.Reviews.RefreshableFooterHeight), activityIndicator: activityIndicator)
        refreshFooter.addSubview(activityIndicator)

        reviewsTableView.configRefreshFooter(with: refreshFooter, container: self) { [unowned self] in
            self.viewModel.loadMore()
        }
    }

    private func handleError(error: Error) {
        UIAlertControllerUtils.showErrorAlert(inViewController: self, title: Constants.GeneralAppProperties.ErrorAlert.Title, message: error.localizedDescription)
    }
}

// MARK: - Data Binding Methods

extension ReviewsViewController {
    private func setupDataBinding() {
        bindReviews()
        bindScrollsToTop()
        bindIsLoading()
        bindErrors()
    }
    
    private func bindReviews() {
        viewModel.reviews.subscribe(onNext: { [unowned self] _ in
            self.reviewsTableView.switchRefreshFooter(to: .normal)
            self.reviewsTableView.reloadData()
        }, onError: { error in
            self.handleError(error: error)
        }).disposed(by: disposeBag)
    }

    private func bindScrollsToTop() {
        viewModel.scrollsToTop.subscribe(onNext: { [unowned self] scrollsToTop in
            if scrollsToTop {
                self.reviewsTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
        }).disposed(by: disposeBag)
    }

    private func bindIsLoading() {
        viewModel.isLoading.subscribe(onNext: { [unowned self] isLoading in
            isLoading ? self.reviewsTableView.refreshControl?.beginRefreshing() : self.reviewsTableView.refreshControl?.endRefreshing()
        }).disposed(by: disposeBag)
    }

    private func bindErrors() {
        viewModel.error.subscribe(onNext: { [unowned self] error in
            self.handleError(error: error)
        }).disposed(by: disposeBag)
    }
}

// MARK: - Private Methods and Properties

extension ReviewsViewController {
    private var sortAlertActions: [UIAlertAction] {
        let dismissAction = UIAlertAction(title: Constants.ViewControllers.Reviews.SortActionSheet.DismissActionTitle, style: .cancel, handler: nil)
        let sortyByDateAction = UIAlertAction(title: Constants.ViewControllers.Reviews.SortActionSheet.SortByDateTitle, style: .default) { [unowned self] _ in
            self.viewModel.sortBy(sortType: .dateOfReview)
        }
        let sortyByRatingAction = UIAlertAction(title: Constants.ViewControllers.Reviews.SortActionSheet.SortByRatingTitle, style: .default) { [unowned self] _ in
            self.viewModel.sortBy(sortType: .rating)
        }
        return [sortyByDateAction, sortyByRatingAction, dismissAction]
    }
}

// MARK: - Selector Methods

extension ReviewsViewController {
    @objc private func refresh() {
        viewModel.refresh()
    }

    @objc private func didTapSortBarButtonItem() {
        UIAlertControllerUtils.showActionSheet(inViewController: self, title: "", message: Constants.ViewControllers.Reviews.SortActionSheet.Message, actions: sortAlertActions)
    }
}
