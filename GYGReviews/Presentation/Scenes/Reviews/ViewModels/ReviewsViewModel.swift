//
//  ReviewsViewModel.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 29.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation
import RxSwift

class ReviewsViewModel: BaseViewModel {

    // MARK: - Properties

    private var reviewsDataSource: ReviewsDataSource
    private var schedulersFacade: SchedulersFacade
    private var disposeBag: DisposeBag
    private var reviewsVariable = Variable<[Review]>([])
    private var reviewsRequestVariable = Variable<ReviewsRequest>(ReviewsRequest(count: Constants.ViewControllers.Reviews.ReviewsPerPage, page: 0))
    var scrollsToTop = PublishSubject<Bool>()

    var reviews: Observable<[Review]> {
        return reviewsVariable.asObservable()
    }

    var numberOfSections: Int {
        return 1
    }

    var numberOfRows: Int {
        return reviewsVariable.value.count
    }

    // MARK: - Initializer

    init(reviewsDataSource: ReviewsDataSource, schedulersFacade: SchedulersFacade, disposeBag: DisposeBag) {
        self.reviewsDataSource = reviewsDataSource
        self.schedulersFacade = schedulersFacade
        self.disposeBag = disposeBag
    }

    // MARK: - Public Methods

    func setupReviewsRequestObserver() {
        reviewsRequestVariable.asObservable()
            .subscribeOn(schedulersFacade.backgroundScheduler())
            .observeOn(schedulersFacade.mainScheduler())
            .subscribe(onNext: { reviewsRequest in
                self.isLoading.onNext(true)
                self.loadReviews(reviewsRequest: reviewsRequest)
            }, onError: { error in
                self.error.onNext(error)
            }).disposed(by: disposeBag)
    }

    func review(for row: Int) -> Review? {
        guard reviewsVariable.value.count > row else { return nil }
        return reviewsVariable.value[row]
    }

    func sortBy(sortType: ReviewsSortType) {
        reviewsRequestVariable.value.sortType = sortType
        refresh()
    }
}

// MARK: - ViewModelNetworking Methods

extension ReviewsViewModel: ViewModelNetworking {
    func refresh() {
        isLoading.onNext(true)
        reviewsRequestVariable.value.page = 0
        reviewsRequestVariable.value.dataLoadingType = .normal
        loadReviews(reviewsRequest: reviewsRequestVariable.value)
    }

    func loadMore() {
        reviewsRequestVariable.value.page += 1
        reviewsRequestVariable.value.dataLoadingType = .infinite
        loadReviews(reviewsRequest: reviewsRequestVariable.value)
    }

    func tryAgain() {
        isLoading.onNext(true)
        reviewsRequestVariable.value.dataLoadingType = .normal
        loadReviews(reviewsRequest: reviewsRequestVariable.value)
    }
}

// MARK: - Private Methods

extension ReviewsViewModel {
    private func loadReviews(reviewsRequest: ReviewsRequest) {
        reviewsDataSource.reviews(reviewsRequest: reviewsRequest)
            .subscribeOn(schedulersFacade.backgroundScheduler())
            .observeOn(schedulersFacade.mainScheduler())
            .subscribe(onNext: { reviews in
                switch reviewsRequest.dataLoadingType {
                case .normal:
                    self.reviewsVariable.value = reviews
                    self.scrollsToTop.onNext(true)
                case .infinite:
                    self.reviewsVariable.value.append(contentsOf: reviews)
                    self.scrollsToTop.onNext(false)
                }
            }, onError: { error in
                self.error.onNext(error)
                self.isLoading.onNext(false)
            }, onCompleted: {
                self.isLoading.onNext(false)
            }).disposed(by: disposeBag)
    }
}
