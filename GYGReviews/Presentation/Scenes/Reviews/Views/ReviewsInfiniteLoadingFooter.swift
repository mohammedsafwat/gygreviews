//
//  ReviewsInfiniteLoadingFooter.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit
import PullToRefreshKit

class ReviewsInfiniteLoadingFooter: UIView, RefreshableFooter {

    // MARK: - Properties

    private var activityIndicator: ActivityIndicator

    // MARK: - Initializer

    init(frame: CGRect, activityIndicator: ActivityIndicator) {
        self.activityIndicator = activityIndicator
        activityIndicator.translatesAutoresizingMaskIntoConstraints(translates: true)
        super.init(frame: frame)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        activityIndicator.setCenter(center: center)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func heightForFooter() -> CGFloat {
        return Constants.ViewControllers.Reviews.RefreshableFooterHeight
    }

    func didUpdateToNoMoreData() {
        activityIndicator.stopAnimating()
    }

    func didResetToDefault() {
        activityIndicator.stopAnimating()
    }

    func didEndRefreshing() {
        activityIndicator.stopAnimating()
    }

    func didBeginRefreshing() {
        activityIndicator.startAnimating()
    }

    func shouldBeginRefreshingWhenScroll() -> Bool {
        return true
    }
}
