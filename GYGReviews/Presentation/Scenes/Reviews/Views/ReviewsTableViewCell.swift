//
//  ReviewsTableViewCell.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 30.10.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit

class ReviewsTableViewCell: UITableViewCell {

    // MARK: - Properties

    @IBOutlet weak var cardView: UIView!
    @IBOutlet private weak var reviewTitleLabel: UILabel!
    @IBOutlet private weak var ratingHorizontalStackView: UIStackView!
    @IBOutlet private weak var reviewContentTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        setupCardViewCornerRadius()
        setupCardViewShadow()
    }

    func configure(withReview review: Review?) {
        guard let review = review else { return }

        reviewTitleLabel.text = review.titleFormatted
        configureRatingStars(rating: review.ratingAsInt)
        reviewContentTextView.text = review.messageFormatted
    }
}

// MARK: - Private Methods

extension ReviewsTableViewCell {
    private func configureRatingStars(rating: Int) {
        ratingHorizontalStackView.arrangedSubviews.enumerated().forEach { (ratingStarIndex, ratingStarView) in
            ratingStarView.isHidden = ratingStarIndex < rating ? false : true
        }
    }

    private func setupCardViewCornerRadius() {
        cardView.setRoundedCorners(cornerRadius: Constants.ViewControllers.Reviews.TableViewCell.CornerRadius)
    }

    private func setupCardViewShadow() {
        let radius = Constants.ViewControllers.Reviews.TableViewCell.CardShadowRadius
        let opacity = Constants.ViewControllers.Reviews.TableViewCell.CardShadowOpacity
        let offset = Constants.ViewControllers.Reviews.TableViewCell.CardShadowOffset
        let shadowColor = UIColor(hexString: Constants.ViewControllers.Reviews.TableViewCell.CardShadowColorHex)
            .withAlphaComponent(Constants.ViewControllers.Reviews.TableViewCell.CardShadowColorAlpha)
        let shadow = Shadow(radius: radius, opacity: opacity, offset: offset, masksToBounds: false, shadowColor: shadowColor)
        cardView.addShadow(shadow: shadow)
    }
}
