//
//  UIAlertControllerUtils.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit

class UIAlertControllerUtils {
    static func showErrorAlert(inViewController viewController: UIViewController, title: String, message: String) {
        let dismissAction = UIAlertAction(title: Constants.GeneralAppProperties.ErrorAlert.DismissActionTitle, style: .default, handler: nil)
        let alert = Alert(viewController: viewController, title: title, message: message, textColorHex: Constants.GeneralAppProperties.ErrorAlert.TextColorHex, textFontName: Constants.GeneralAppProperties.ErrorAlert.TextFontName, textFontSize: Constants.GeneralAppProperties.ErrorAlert.TextFontSize, style: .alert, actions: [dismissAction])
        alert.show()
    }

    static func showActionSheet(inViewController viewController: UIViewController, title: String, message: String, actions: [UIAlertAction]) {
        let alert = Alert(viewController: viewController, title: title, message: message, textColorHex: Constants.ViewControllers.Reviews.SortActionSheet.TextColorHex, textFontName: Constants.ViewControllers.Reviews.SortActionSheet.TextFontName, textFontSize: Constants.ViewControllers.Reviews.SortActionSheet.TextFontSize, style: .actionSheet, actions: actions)
        alert.show()
    }
}
