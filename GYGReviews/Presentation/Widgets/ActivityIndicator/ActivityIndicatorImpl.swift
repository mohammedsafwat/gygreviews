//
//  ActivityIndicatorImpl.swift
//  GYGReviews
//
//  Created by Mohammed Safwat on 08.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

class ActivityIndicatorImpl: UIView, ActivityIndicator {
    private var nvActivityIndicatorView: NVActivityIndicatorView

    init(frame: CGRect, type: ActivityIndicatorType, color: UIColor?, padding: CGFloat?) {
        nvActivityIndicatorView = NVActivityIndicatorView(frame: frame, type: type == .ballBeat ? NVActivityIndicatorType.ballBeat : NVActivityIndicatorType.ballClipRotate, color: color, padding: padding)
        super.init(frame: frame)
        addSubview(nvActivityIndicatorView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func startAnimating() {
        nvActivityIndicatorView.startAnimating()
    }

    func stopAnimating() {
        nvActivityIndicatorView.stopAnimating()
    }

    func translatesAutoresizingMaskIntoConstraints(translates: Bool) {
        nvActivityIndicatorView.translatesAutoresizingMaskIntoConstraints = translates
    }

    func setCenter(center: CGPoint) {
        nvActivityIndicatorView.center = center
    }
}
