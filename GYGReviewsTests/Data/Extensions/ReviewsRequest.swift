//
//  ReviewsRequest.swift
//  GYGReviewsTests
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Mimus
@testable import GYGReviews

extension ReviewsRequest: MockEquatable {
    public func equalTo(other: Any?) -> Bool {
        if let otherReviewsRequest = other as? ReviewsRequest {
            return self.equals(reviewsRequest: otherReviewsRequest)
        }
        return false
    }
}
