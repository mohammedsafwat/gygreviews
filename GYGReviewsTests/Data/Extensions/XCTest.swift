//
//  XCTest.swift
//  GYGReviewsTests
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import XCTest

extension XCTest {
    func stubbedData(fromJSONFile jsonFileName: String) -> Data {
        let path = Bundle(for: type(of: self)).path(forResource: jsonFileName, ofType: "json")
        let nsdata = NSData(contentsOfFile: path!)
        return Data(referencing: nsdata!)
    }
}
