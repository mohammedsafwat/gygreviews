//
//  MockRestNetworkClient.swift
//  GYGReviewsTests
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import RxSwift

@testable import GYGReviews

enum StubbedDataStatusType {
    case success
    case failure
}

class MockRestNetworkClient: RestNetworkClientProtocol {
    private var stubbedData: Any
    private var stubbedDataStatusType: StubbedDataStatusType

    init(stubbedData: Any, stubbedDataStatusType: StubbedDataStatusType) {
        self.stubbedData = stubbedData
        self.stubbedDataStatusType = stubbedDataStatusType
    }

    func performRequest(requestURLString: String, type: HTTPRequestType, parameters: [String: Any]) -> Observable<Result<(HTTPURLResponse, Any), DataError>> {
        switch self.stubbedDataStatusType {
        case .success:
            let resultData = stubbedData as? Data
            let resultJSON = TestingUtils.json(fromData: resultData!)
            let result = (HTTPURLResponse(url: URL(string: requestURLString)!, statusCode: 200, httpVersion: nil, headerFields: nil)!, resultJSON as Any) as (HTTPURLResponse, Any)
            return Observable.just(.success(result))
        case .failure:
            let error = DataErrorHelper.requestFailedError
            return Observable.just(.failure(error))
        }
    }
}
