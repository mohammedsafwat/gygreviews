//
//  MockReviewsDataSource.swift
//  GYGReviewsTests
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Mimus
import RxSwift
import RxTest
@testable import GYGReviews

class MockReviewsDataSource: ReviewsDataSource, Mock {
    var storage: [RecordedCall] = []

    let reviews: Observable<[Review]>

    init(reviews: Observable<[Review]>) {
        self.reviews = reviews
    }

    func reviews(reviewsRequest: ReviewsRequest) -> Observable<[Review]> {
        recordCall(withIdentifier: "reviews", arguments: [reviewsRequest])
        return reviews
    }
}
