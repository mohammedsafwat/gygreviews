//
//  DataModule.swift
//  GYGReviewsTests
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import RxSwift
import RxTest
@testable import GYGReviews

class DataModule {
    static let shared = DataModule()
    private let testScheduler = TestScheduler(initialClock: 0)
    private let mockReviewsRequestCount = 20
    private let mockReviewsRequestPage = 0

    let mockedReviews = [
        Review(id: 3520187,
               rating: "1.0",
               title: "Sono arrivato in ritardo e non ho fatto la visita",
               message: "Colpa mia...colpa della applicazione che mi ha fatto scendere a 3km di distanza...colpa dei berlinesi che in un posto sterminato hanno messo un entrata introvabile e insulsa oltre che non segnalata...colpa vostra che sulla prenotazione non avete scritto nulla di più perché risultasse intellegibile...insomma il museo sarà anche interessante ma la zona è brutta senza se e senza ma...",
               author: "Pierluigi – Italy",
               foreignLanguage: true,
               date: "August 25, 2018",
               languageCode: "it",
               travelerType: "solo",
               reviewerName: "Pierluigi",
               reviewerCountry: "Italy"),
        Review(id: 2366352,
               rating: "1.0",
               title: "Geht so...",
               message: "Hätte mir mehr Infos über die Entstehung und Nutzung durchs NS Regime gewünscht... Unser Guide wirkte streckenweise etwas unmotiviert. Als absolut negativ empfand ich, dass die Tour laut Guide nicht unterbrochen werden kann. Für jemanden der Höhenangst hat, nicht so günstig. Fazit: Panikatacke...",
               author: "Andreas – Geesthacht, Germany",
               foreignLanguage: true,
               date: "April 21, 2018",
               languageCode: "en",
               travelerType: "couple",
               reviewerName: "Andreas",
               reviewerCountry: "Geesthacht, Germany")
    ]

    func mockReviewsDataSource() -> MockReviewsDataSource {
        return MockReviewsDataSource(reviews: Observable.just(mockedReviews))
    }

    func mockReviewsRequest() -> ReviewsRequest {
        return ReviewsRequest(count: 20, page: 0)
    }
}
