//
//  ReviewsRemoteDataSourceSpec.swift
//  GYGReviewsTests
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Quick
import Nimble
import RxSwift
import RxBlocking

@testable import GYGReviews

class ReviewsRemoteDataSourceSpec: QuickSpec {
    private var reviewsRemoteDataSource: ReviewsRemoteDataSource!

    override func spec() {
        super.spec()

        describe("reviews remote data source") {
            context("fetching reviews data with valid non empty response") {
                beforeEach {
                    let reviewsStubbedJSON = self.stubbedData(fromJSONFile: "ReviewsValidNonEmptyResponse")
                    let mockRestNetworkClient = MockRestNetworkClient(stubbedData: reviewsStubbedJSON, stubbedDataStatusType: .success)
                    self.reviewsRemoteDataSource = ReviewsRemoteDataSource(restNetworkClient: mockRestNetworkClient)
                }

                it("should parse the reviews response data correctly") {
                    let response = self.reviewsRemoteDataSource.reviews(reviewsRequest: DataModule.shared.mockReviewsRequest()).toBlocking().materialize()

                    switch response {
                    case .completed(let reviews):
                        expect(reviews.count).to(beGreaterThan(0))
                        self.verifyExpectedResponse(review: reviews[0][0])
                    case .failed:
                        fail("Expected reviews request to complete without errors")
                    }
                }
            }

            context("fetching reviews data with valid empty response") {
                beforeEach {
                    let reviewsStubbedJSON = self.stubbedData(fromJSONFile: "ReviewsValidEmptyResponse")
                    let mockRestNetworkClient = MockRestNetworkClient(stubbedData: reviewsStubbedJSON, stubbedDataStatusType: .success)
                    self.reviewsRemoteDataSource = ReviewsRemoteDataSource(restNetworkClient: mockRestNetworkClient)
                }

                it("should return an empty reviews array") {
                    let response = self.reviewsRemoteDataSource.reviews(reviewsRequest: DataModule.shared.mockReviewsRequest()).toBlocking().materialize()

                    switch response {
                    case .completed(let reviews):
                        expect(reviews[0].count).to(equal(0))
                    case .failed:
                        fail("Expected reviews request to complete without errors")
                    }
                }
            }

            context("fetching reviews data with invalid response") {
                beforeEach {
                    let reviewsStubbedJSON = self.stubbedData(fromJSONFile: "ReviewsInvalidResponse")
                    let mockRestNetworkClient = MockRestNetworkClient(stubbedData: reviewsStubbedJSON, stubbedDataStatusType: .failure)
                    self.reviewsRemoteDataSource = ReviewsRemoteDataSource(restNetworkClient: mockRestNetworkClient)
                }

                it("should return an error") {
                    let response = self.reviewsRemoteDataSource.reviews(reviewsRequest: DataModule.shared.mockReviewsRequest()).toBlocking().materialize()

                    switch response {
                    case .completed:
                        fail("Expected reviews request to complete with an error")
                    case .failed(let error):
                        expect(error.error).to(beAKindOf(DataError.self))
                    }
                }
            }
        }
    }
}

// MARK: - Private Methods

extension ReviewsRemoteDataSourceSpec {
    private func verifyExpectedResponse(review: Review) {
        let expectedResponse = DataModule.shared.mockedReviews[0]
        expect(review.id).to(equal(expectedResponse.id))
        expect(review.rating).to(equal(expectedResponse.rating))
        expect(review.title).to(equal(expectedResponse.title))
        expect(review.message).to(equal(expectedResponse.message))
        expect(review.author).to(equal(expectedResponse.author))
        expect(review.foreignLanguage).to(equal(expectedResponse.foreignLanguage))
        expect(review.date).to(equal(expectedResponse.date))
        expect(review.languageCode).to(equal(expectedResponse.languageCode))
        expect(review.travelerType).to(equal(expectedResponse.travelerType))
        expect(review.reviewerName).to(equal(expectedResponse.reviewerName))
        expect(review.reviewerCountry).to(equal(expectedResponse.reviewerCountry))
    }
}
