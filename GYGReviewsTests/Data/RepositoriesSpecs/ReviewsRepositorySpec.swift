//
//  ReviewsRepositorySpec.swift
//  GYGReviewsTests
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Quick
import Nimble
import RxSwift
import RxBlocking

@testable import GYGReviews

class ReviewsRepositorySpec: QuickSpec {
    private var reviewsRepository: ReviewsRepository!
    private let mockRemoteDataSource = DataModule.shared.mockReviewsDataSource()

    override func spec() {
        super.spec()
        
        describe("reviews repository") {
            context("when `reviews` is called") {
                beforeEach {
                    self.reviewsRepository = ReviewsRepository(remoteDataSource: self.mockRemoteDataSource)
                }

                it("should call `reviews` on remoteDataSource") {
                    _ = self.reviewsRepository.reviews(reviewsRequest: DataModule.shared.mockReviewsRequest()).toBlocking().materialize()
                    self.mockRemoteDataSource.verifyCall(withIdentifier: "reviews", arguments: [DataModule.shared.mockReviewsRequest()])
                }

                it("should return a response of type `Observable<[Review]>`") {
                    let response = self.reviewsRepository.reviews(reviewsRequest: DataModule.shared.mockReviewsRequest()).toBlocking().materialize()

                    switch response {
                    case .completed(let reviews):
                        expect(reviews[0]).to(beAKindOf([Review].self))
                    case .failed:
                        expect(response).toNot(beAKindOf([Review].self))
                    }
                }
            }
        }
    }
}
