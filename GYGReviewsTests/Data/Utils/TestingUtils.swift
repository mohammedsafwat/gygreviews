//
//  TestingUtils.swift
//  GYGReviewsTests
//
//  Created by Mohammed Safwat on 09.11.18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation
import SwiftyJSON

class TestingUtils {
    static func json(fromData data: Data) -> [String: Any]? {
        do {
            return try JSON(data: data).dictionaryObject
        } catch {
            return nil
        }
    }
}
