GYG Reviews
===

This is a small iOS demo application that allows browsing reviews for one of
GYG's [most popular Berlin tours](https://www.getyourguide.com/berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/)

## How to run

Clone the project directory, then in the root of your directory run `pod install`.
After that, open the workspace file. You can run both the application and the
unit tests targets after that.

## Screenshots
![Screenshot](https://i.imgur.com/UiTloC7.png)
![Screenshot](https://i.imgur.com/gwRYa00.png)
